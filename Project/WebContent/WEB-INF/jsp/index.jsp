<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ログイン画面</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />

</head>
	<body>
        <header>
            <h1>ログイン画面</h1>
        </header>


        	<main>

      		<c:if test="${errMsg != null}" >
	    		<div class="alert">
		  			${errMsg}
				</div>

			</c:if>

            <div class="login-area">

            <form action="LoginServlet2" method="post">

                <table>
                <tr>
                    <th><strong>ログインID</strong></th>
                    <th><input type="text" name="loginId" class="input"></th>
                </tr>

                <tr>
                    <th><strong>パスワード</strong></th>
                    <th><input type="password" name="password" class="input"></th>
                </tr>
                </table>

                <p><input type="submit" value="ログイン" class="button"></p>

            </form>

			</div>

        </main>

	</body>
</html>
