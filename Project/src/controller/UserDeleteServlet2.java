package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao2;
import model.User2;

/**
 * Servlet implementation class UserDeleteServlet2
 */
public class UserDeleteServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		 if (session.getAttribute("userInfo") == null){
			 response.sendRedirect("LoginServlet2");
			 return;
		 }

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);

		// idからユーザー情報を取得
		UserDao2 userDao2 = new UserDao2();
		User2 user2 = userDao2.findById2(id);

		// ユーザー情報をセッションスコープにセット
		session.setAttribute("userDetail", user2);


		//forwardで<delete.jsp>を出力
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/delete.jsp");
		dispatcher.forward(request, response);
		return;
		}



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	// 削除ボタンを押したとき
		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		// idを引数に渡してDaoでDELETEを実行
		UserDao2 userDao2 = new UserDao2();
		User2 user2 = userDao2.deleteData(id);

		// ユーザ一覧のサーブレットにリダイレクト
		response.sendRedirect("UserListServlet2");
	}

}
