<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ情報詳細参照</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />

</head>
	<body>
        <header>
            <div class="header-area">
                <p class="p1">${userInfo.name} さん</p>
                <p class="p2"><a href="LogoutServlet2">ログアウト</a></p>
            </div>
            <h1>ユーザ情報詳細参照</h1>
        </header>

        <main>
            <div class="register-area">
                <table>
                <tr>
                    <th><strong>ログインID</strong></th>
                    <th>${userDetail.loginId}</th>
                </tr>

                <tr>
                    <th><strong>ユーザ名</strong></th>
                    <th>${userDetail.name}</th>
                </tr>
                    <tr>
                    <th><strong>生年月日</strong></th>
                    <th>${userDetail.birthDate}</th>
                </tr>
                    <tr>
                    <th><strong>登録日時</strong></th>
                    <th>${userDetail.createDate}</th>
                </tr>
                    <tr>
                    <th><strong>更新日時</strong></th>
                    <th>${userDetail.updateDate}</th>
                </tr>
                </table>

               </div>

            <p class="p3"><a href="UserListServlet2">戻る</a></p>

        </main>

	</body>
</html>
