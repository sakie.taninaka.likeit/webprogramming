package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao2;
import model.User2;

/**
 * Servlet implementation class UserUpdateServlet2
 */
public class UserUpdateServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		 if (session.getAttribute("userInfo") == null){
			 response.sendRedirect("LoginServlet2");
			 return;
		 }

		// 更新ボタンを押したとき
		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);

		// idからユーザー情報を取得
		UserDao2 userDao2 = new UserDao2();
		User2 user2 = userDao2.findById2(id);

		// ユーザー情報をリクエストスコープにセット
		session.setAttribute("userDetail", user2);


		//forwardで<update.jsp>を出力
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの入力項目を取得
		request.setCharacterEncoding("UTF-8");

		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		//パスワードが一致しないとき


		//未入力の欄があるとき
		if(name.isEmpty() || birthDate.isEmpty()) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "未入力の欄があります");

			// update.jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response);
			return;
		}if(!password.equals(password2)){
			// 取得した入力項目(パスワード以外)をリクエストスコープにセット
			request.setAttribute("name", name);
			request.setAttribute("birthDate", birthDate);

			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "パスワードとパスワード(確認)が一致しません");

			// update.jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response);
			return;

		//パスワードが両方未入力のとき
		}if(password.isEmpty() && password2.isEmpty()){
			// URLからGETパラメータとしてIDを受け取る
			String id = request.getParameter("id");

			//リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
			UserDao2 userDao2 = new UserDao2();
			userDao2.updateData2(name, birthDate, id);


			// ユーザ一覧のサーブレットにリダイレクト
			response.sendRedirect("UserListServlet2");
			return;



		}


		//更新に成功したとき
		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		//リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao2 userDao2 = new UserDao2();
		userDao2.updateData(password, name, birthDate, id);




		// ユーザ一覧のサーブレットにリダイレクト
		response.sendRedirect("UserListServlet2");

		}


}
