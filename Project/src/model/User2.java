package model;

import java.io.Serializable;

public class User2 implements Serializable{
	private int id;
	private String loginId;
	private String name;
	private String birthDate;
	private String password;
	private String createDate;
	private String updateDate;

	//ログインセッションを保存するためのコンストラクタ
	public User2 (String loginId, String name) {
		this.loginId = loginId;
		this.name = name;
	}

	// 全てのデータをセットするコンストラクタ
	public User2(int id, String loginId, String name, String birthDate, String password, String createDate,
			String updateDate) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.password = password;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	//新規登録時にログインIDを探し、それを保存するコンストラクタ
	public User2 (String loginId) {
		this.loginId = loginId;
	}

	// 新規登録失敗時に、入力した内容を保存するコンストラクタ
	public User2 (String loginId, String name, String birthDate) {
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
	}

	// 新規登録成功時に、入力した内容を保存するコンストラクタ
	public User2 (String loginId, String name, String birthDate, String password) {
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.password = password;

	}


	// ユーザの詳細情報をセットするコンストラクタ
	public User2(String loginId, String name, String birthDate, String createDate, String updateDate) {
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	// ユーザの詳細情報をセットするコンストラクタ
	public User2(String password, String name, String birthDate, int id) {
		this.password = password;
		this.name = name;
		this.birthDate = birthDate;
		this.id = id;
	}

	// パスワード以外のユーザの情報を更新する
	public User2(String name, String birthDate, int id) {
		this.name = name;
		this.birthDate = birthDate;
		this.id = id;
		}

	//アクセサ
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String creasteDate) {
		this.createDate = createDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}





}
