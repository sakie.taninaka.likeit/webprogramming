CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;

USE usermanagement;

CREATE TABLE user(
id SERIAL PRIMARY KEY AUTO_INCREMENT,
login_id varchar(255) UNIQUE NOT NULL,
name varchar(255) NOT NULL,
birth_date DATE NOT NULL,
password varchar(255) NOT NULL,
create_date DATETIME NOT NULL,
update_date DATETIME NOT NULL);

INSERT INTO user(login_id,name,password,birth_date,create_date,update_date) VALUES('admin','�Ǘ���','password','2020-01-01',NOW(),NOW());
INSERT INTO user(login_id,name,password,birth_date,create_date,update_date) VALUES('id0001','�c�����Y','password0001','1989-04-26',NOW(),NOW());
INSERT INTO user(login_id,name,password,birth_date,create_date,update_date) VALUES('id0002','������Y','password0002','2001-11-12',NOW(),NOW());
INSERT INTO user(login_id,name,password,birth_date,create_date,update_date) VALUES('id0003','����^�i','password0003','2000-01-01',NOW(),NOW());


SELECT * FROM user WHERE login_id ='admin' and password = 'password';

SELECT * FROM user WHERE login_id = 'admin' and password = 'hoge';
SELECT * FROM user;

DELETE FROM user WHERE id = 5;
