package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao2;
import model.User2;

/**
 * Servlet implementation class UserListServlet2
 */

public class UserListServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserListServlet2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		 if (session.getAttribute("userInfo") == null){
			 response.sendRedirect("LoginServlet2");
			 return;
		 }



		// ユーザ一覧情報を取得
		UserDao2 userDao2 = new UserDao2();
		List<User2> userList = userDao2.findAll();

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userList", userList);

		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/users.jsp");
		dispatcher.forward(request, response);
		return;

		}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	// TODO  未実装：検索処理全般
		// リクエストパラメータの入力項目を取得
		request.setCharacterEncoding("UTF-8");

		String loginIdP = request.getParameter("loginId");
		String nameP = request.getParameter("name");
		String birthDateStart = request.getParameter("birthDateStart");
		String birthDateEnd = request.getParameter("birthDateEnd");

		// Daoで検索処理を実行
		UserDao2 userDao2 = new UserDao2();
		List<User2> userList = userDao2.findByName(loginIdP,nameP,birthDateStart,birthDateEnd);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userList", userList);

		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/users.jsp");
		dispatcher.forward(request, response);
		return;



	}

}
