<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ情報更新</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />

</head>
	<body>
        <header>
            <div class="header-area">
                <p class="p1">${userInfo.name} さん</p>
                <p class="p2"><a href="LogoutServlet2">ログアウト</a></p>
            </div>
            <h1>ユーザ情報更新</h1>
        </header>

        <main>
            <div class="register-area">

            <div class="alert">
		  		${errMsg}
			</div>

            <form action="UserUpdateServlet2" method="post">
            	<input type="hidden" name="id" value="${userDetail.id}">
                <table>
                <tr>
                    <th><strong>ログインID</strong></th>
                    <th>${userDetail.loginId}</th>
                </tr>

                <tr>
                    <th><strong>パスワード</strong></th>
                    <th><input type="password" name="password" class="input"></th>
                </tr>
                    <tr>
                    <th><strong>パスワード(確認)</strong></th>
                    <th><input type="password" name="password2" class="input"></th>
                </tr>
                    <tr>
                    <th><strong>ユーザ名</strong></th>
                    <th><input type="text" name="name"  value="${userDetail.name}" class="input"></th>
                </tr>
                    <tr>
                    <th><strong>生年月日</strong></th>
                    <th><input type="date" name="birthDate"  value="${userDetail.birthDate}"  placeholder="1989 / 04 / 26" class="input"></th>
                </tr>
                </table>
                <p><input type="submit" value="登録" class="button"></p>

                </form>
               </div>

            <p class="p3"><a href="UserListServlet2">戻る</a></p>

        </main>

	</body>
</html>
