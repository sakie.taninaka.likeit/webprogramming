package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao2;
import model.User2;

/**
 * Servlet implementation class userCreateServlet2
 */
public class userCreateServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public userCreateServlet2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		 if (session.getAttribute("userInfo") == null){
			 response.sendRedirect("LoginServlet2");
			 return;
		 }

		//forwardで<register.jsp>を出力
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/register.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// リクエストパラメータの入力項目を取得
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");


		//リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao2 userDao2 = new UserDao2();
		User2 user2 = userDao2.findById(loginId);

		//ログインIDとテーブルデータが一致したとき
		if(user2 != null) {
			// 取得した入力項目(パスワード以外)をリクエストスコープにセット
			request.setAttribute("loginId", loginId);
			request.setAttribute("name", name);
			request.setAttribute("birthDate", birthDate);

			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力されたログインIDはすでに登録されています");

			// register.jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/register2.jsp");
			dispatcher.forward(request, response);
			return;


		//パスワードが一致しないとき
		}if(!password.equals(password2)){
			// 取得した入力項目(パスワード以外)をリクエストスコープにセット
			request.setAttribute("loginId", loginId);
			request.setAttribute("name", name);
			request.setAttribute("birthDate", birthDate);

			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "パスワードとパスワード(確認)が一致しません");

			// register2.jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/register2.jsp");
			dispatcher.forward(request, response);
			return;

		//未入力の欄があるとき
		}if(loginId.isEmpty()|| name.isEmpty() || birthDate.isEmpty() || password.isEmpty()) {
			// 取得した入力項目(パスワード以外)をリクエストスコープにセット
			request.setAttribute("loginId", loginId);
			request.setAttribute("name", name);
			request.setAttribute("birthDate", birthDate);

			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "未入力の欄があります");

			// register2.jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/register2.jsp");
			dispatcher.forward(request, response);
			return;
		}


		//新規登録に成功したとき
			//リクエストパラメータの入力項目を引数に渡して、DaoでInsertする処理を実行
			userDao2.insertData(loginId, name, birthDate, password);

			// ユーザ一覧のサーブレットにリダイレクト
			response.sendRedirect("UserListServlet2");



	}

}


