package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User2;

/**
 * ユーザテーブル用のDao
 * @author takano
 *
 */
public class UserDao2 {

    /**
     * ログインIDとパスワードに紐づくユーザ情報を返す
     * @param loginId
     * @param password
     * @return
     */
    public User2 findByLoginInfo(String loginId, String password) {
        Connection conn = null;
        String result = encodePass(password);
        try {
            // データベースへ接続
            conn = DBManager2.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, result);
            ResultSet rs = pStmt.executeQuery();

            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            return new User2(loginIdData, nameData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }


    /**
     * 全てのユーザ情報を取得する
     * @return
     */
    public List<User2> findAll() {
        Connection conn = null;
        List<User2> userList = new ArrayList<User2>();

        try {
            // データベースへ接続
            conn = DBManager2.getConnection();

            // SELECT文を準備
            // 管理者以外を取得するようSQLを変更する
            String sql = "SELECT * FROM user WHERE id != 1";

             // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                String birthDate = rs.getString("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User2 user = new User2(id, loginId, name, birthDate, password, createDate, updateDate);

                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }




    /**
     * ログインIDに紐づくユーザ情報を返す
     * @param loginId
     * @return
     */
    public User2 findById(String loginId) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager2.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            String loginId2 = rs.getString("id");

            return new User2(loginId2);


        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }


    /**
     * 新規ユーザー登録
     * @return
     */
    public void insertData(String loginId, String name, String birthDate, String password) {
    	String result = encodePass(password);
        Connection conn = null;

        try {
            // データベースへ接続
            conn = DBManager2.getConnection();

            // INSERT文を準備
            String sql = "INSERT INTO user(login_id, name, birth_date, password,create_date,update_date) values(?, ?, ?, ?,now(),now())";

             // INSERTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, name);
            pStmt.setString(3, birthDate);
            pStmt.setString(4, result);

            pStmt.executeUpdate();


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * ユーザーの詳細情報を返す
     * @return
     */

    public User2 findById2(String id) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager2.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE id = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, id);
            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行Sう
            if (!rs.next()) {
                return null;
            }

         // 必要なデータのみインスタンスのフィールドに追加
            int id2 = rs.getInt("id");
            String loginId = rs.getString("login_id");
            String name = rs.getString("name");
            String birthDate = rs.getString("birth_date");
            String password = rs.getString("password");
            String createDate = rs.getString("create_date");
            String updateDate = rs.getString("update_date");
            User2 user = new User2(id2, loginId, name, birthDate, password, createDate, updateDate);

            return user;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }
    /**
     * ユーザー情報更新
     * @return
     */
    public User2 updateData(String password, String name, String birthDate, String id) {
        Connection conn = null;
        String result = encodePass(password);

        try {
            // データベースへ接続
            conn = DBManager2.getConnection();

            // INSERT文を準備
            String sql = "UPDATE user SET password = ?,name = ?, birth_date = ?, update_date = NOW() WHERE id = ?";

             // INSERTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, result);
            pStmt.setString(2, name);
            pStmt.setString(3, birthDate);
            pStmt.setString(4, id);

            pStmt.executeUpdate();


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
		return null;
    }
    public User2 updateData2(String name, String birthDate, String id) {
        Connection conn = null;

        try {
            // データベースへ接続
            conn = DBManager2.getConnection();

            // INSERT文を準備
            String sql = "UPDATE user SET name = ?, birth_date = ?, update_date = NOW() WHERE id = ?";

             // INSERTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, name);
            pStmt.setString(2, birthDate);
            pStmt.setString(3, id);

            pStmt.executeUpdate();


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
		return null;
    }


    /**
     * ユーザの削除
     * @return
     */
    public User2 deleteData(String id) {
        Connection conn = null;

        try {
            // データベースへ接続
            conn = DBManager2.getConnection();

            // DELETE文を準備
            String sql = "DELETE FROM user WHERE id = ?";

             // DELETEを実行
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, id);
            pStmt.executeUpdate();


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
		return null;
    }

    /**
     * ユーザ名から検索
     * @return
     */

    public List<User2> findByName(String loginIdP,String nameP, String birthDateStart, String birthDateEnd) {

        Connection conn = null;
        List<User2> userList = new ArrayList<User2>();

        try {
            // データベースへ接続
            conn = DBManager2.getConnection();

            // SELECT文を準備
            // 管理者以外を取得するようSQLを変更する
            String sql = "SELECT * FROM user WHERE id != 1";

            if(!loginIdP.equals("")) {
            	sql += " AND login_id = '" + loginIdP + "'";
            }

            if(!nameP.equals("")) {
            	sql += " AND name LIKE '" +'%'+ nameP +'%'+ "'";
            }

            if(!birthDateStart.equals("")) {
            	sql += " AND birth_date >= '" + birthDateStart + "'";
            }

            if(!birthDateEnd.equals("")) {
            	sql += " AND birth_date <= '" + birthDateEnd + "'";
            }

             // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                String birthDate = rs.getString("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User2 user = new User2(id, loginId, name, birthDate, password, createDate, updateDate);

                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;

    }


    private static String encodePass(String password) {
        //ハッシュを生成したい元の文字列
        String source = password;
        //ハッシュ生成前にバイト配列に置き換える際のCharset
        Charset charset = StandardCharsets.UTF_8;
        //ハッシュアルゴリズム
        String algorithm = "MD5";

        //ハッシュ生成処理
        byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
        String result = DatatypeConverter.printHexBinary(bytes);
        //標準出力
        return result;
    }


    /*
    public List<User2> findByName(String name) {
        Connection conn = null;
        List<User2> userList = new ArrayList<User2>();

        try {
            // データベースへ接続
            conn = DBManager2.getConnection();

            // SELECT文を準備
            // 管理者以外を取得するようSQLを変更する
            String sql = "SELECT * FROM user WHERE name LIKE ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, "%" + name +"%");
            ResultSet rs = pStmt.executeQuery();

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name2 = rs.getString("name");
                String birthDate = rs.getString("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User2 user = new User2(id, loginId, name2, birthDate, password, createDate, updateDate);

                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }
    */


}

