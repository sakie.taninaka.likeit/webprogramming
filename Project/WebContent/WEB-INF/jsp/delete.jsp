<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ削除確認</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />

</head>
	<body>
        <header>
            <div class="header-area">
                <p class="p1">${userInfo.name} さん</p>
                <p class="p2"><a href="LogoutServlet2">ログアウト</a></p>
            </div>
            <h1>ユーザ削除確認</h1>
        </header>

        <main>
            <p>ログインID: ${userDetail.loginId}<br>
            を本当に削除してよろしいでしょうか。</p>

            <div class="delete-area">

                <table>
                    <tr>
                        <th><p><a href="UserListServlet2"><input type="submit" value="キャンセル" class="button"></a></p></th>

                        <form action="UserDeleteServlet2" method="post">
                        	<input type="hidden" name="id" value="${userDetail.id}">
                        	<th><p><input type="submit" value="OK" class="button"></p></th>
                        </form>
                    </tr>
                </table>


            </div>
         </main>

	</body>
</html>
