<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ一覧</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />

</head>
	<body>
        <header>
            <div class="header-area">
                <p class="p1">${userInfo.name} さん </p>
                <p class="p2"><a href="LogoutServlet2">ログアウト</a></p>
            </div>
            <h1>ユーザ一覧</h1>
        </header>

        <main>
            <div class="serch-area">
                <p><a href="userCreateServlet2">新規登録</a></p>

                <form action="UserListServlet2" method="post">
                <table>
                <tr>
                    <th><strong>ログインID</strong></th>
                    <th><input type="text" name="loginId" class="input"></th>
                </tr>

                <tr>
                    <th><strong>ユーザ名</strong></th>
                    <th><input type="text" name="name" class="input"></th>
                </tr>
                    <tr>
                    <th><strong>生年月日</strong></th>
                    <th><input type="date" name="birthDateStart" class="input">～
                    <input type="date" name="birthDateEnd" class="input"></th>
                </tr>
                </table>


                <p><input type="submit" value="検索" class="button"></p>
                </form>


            </div>

            <hr>

            <div class="users-area">
                <table>
                <thead>
                    <tr>
                        <th>ログインID</th>
                        <th>ユーザ名</th>
                        <th>生年月日</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="user2" items="${userList}" >

                    <tr>
                        <td>${user2.loginId}</td>
                     	<td>${user2.name}</td>
                     	<td>${user2.birthDate}</td>
                        <td>
                         <c:if test="${userInfo.loginId == 'admin'}" >
	                        	<a href="UserDetailServlet2?id=${user2.id}" type="submit" class="btn1">詳細</a>


	                        	<a href="UserUpdateServlet2?id=${user2.id}" type="submit" class="btn2">更新</a>



	                        	<a href ="UserDeleteServlet2?id=${user2.id}" type="submit"  class="btn3">削除</a>
	                       </c:if>

	                       <c:if test="${userInfo.loginId != 'admin'}" >
	                        	<a href="UserDetailServlet2?id=${user2.id}" type="submit" class="btn1">詳細</a>

	                        	<c:if test="${userInfo.loginId == user2.loginId}" >
	                        		<a href="UserUpdateServlet2?id=${user2.id}" type="submit" class="btn2">更新</a>
	                        	</c:if>

							</c:if>


	                    </td>
                    </tr>

                    </c:forEach>
                    </tbody>

                </table>
            </div>

        </main>

	</body>
</html>
